// (function () {
//     'use strict';
//
//     angular.module('helloCoreApi').controller('mapCtrl', [
//         '$scope',
//         '$http',
//         '$q',
//         'c8yBase',
//         'c8yEvents',
//         'c8yCepModule',
//         '$routeParams',
//         'c8yMeasurements',
//         'c8yInventory',
//         '$cacheFactory',
//         'c8yDevices',
//         mapCtrl
//     ]);
//     function mapCtrl($scope, $http, $q, c8yBase, c8yEvents, c8yCepModule, $routeParams, c8yMeasurements, c8yInventory, $cacheFactory, c8yDevices) {
//         // c8yEvents.list(
//         //     angular.extend(c8yBase.timeOrderFilter(), {})
//         // ).then(function (events) {
//         //     $scope.events = events;
//         //     // console.log($scope.events);
//         //     $scope.arr = [];
//         //     angular.forEach($scope.events, function (data) {
//         //         $scope.arr.push(data.c8y_Position.lng + ',' + data.c8y_Position.lat);
//         //     });
//         //     $scope.map = new AMap.Map('container', {
//         //         resizeEnable: true,
//         //         zoom: 11,
//         //         center: [116.468711, 40.022691]
//         //     });
//         //     $scope.map.setMapStyle("light");
//         //     for (var i = 0; i < $scope.arr.length; i++) {
//         //         $scope.marker = new AMap.Marker({
//         //             map: $scope.map,
//         //             // icon: $scope.icon,//24px*24px'
//         //             position: $scope.arr[i].split(','),
//         //             offset: new AMap.Pixel(0, 0)
//         //         });
//         //     }
//         // });
//
//         c8yMeasurements.list(
//             _.assign(c8yBase.todayFilter(), {
//             })
//         ).then(function (measurements) {
//             $scope.measurements = measurements;
//             console.log($scope.measurements);
//         });
//
//
//         !function(){
//             var infoWindow, map, level = 11,
//                 center = {lng: 116.544468, lat: 39.955549},
//                 features = [{type: "Marker", name: "", desc: "", color: "blue", icon: "cir", offset: {x: -9, y: -31}, lnglat: {lng: 116.436665, lat: 39.917378}},
//                     {type: "Marker", name: "", desc: "", color: "blue", icon: "cir", offset: {x: -9, y: -31}, lnglat: {lng: 116.445076, lat: 39.911848}},
//                     {type: "Marker", name: "", desc: "", color: "blue", icon: "cir", offset: {x: -9, y: -31}, lnglat: {lng: 116.458294, lat: 39.921855}},
//                     {type: "Marker", name: "", desc: "", color: "blue", icon: "cir", offset: {x: -9, y: -31}, lnglat: {lng: 116.447479, lat: 39.92949}}];
//
//             function loadFeatures(){
//                 for(var feature, data, i = 0, len = features.length, j, jl, path; i < len; i++){
//                     data = features[i];
//                     switch(data.type){
//                         case "Marker":
//                             feature = new AMap.Marker({ map: map, position: new AMap.LngLat(data.lnglat.lng, data.lnglat.lat),
//                                 zIndex: 3, extData: data, offset: new AMap.Pixel(data.offset.x, data.offset.y), title: data.name,
//                                 content: '<div class="icon icon-' + data.icon + ' icon-'+ data.icon +'-' + data.color +'"></div>' });
//                             break;
//                         case "Polyline":
//                             for(j = 0, jl = data.lnglat.length, path = []; j < jl; j++){
//                                 path.push(new AMap.LngLat(data.lnglat[j].lng, data.lnglat[j].lat));
//                             }
//                             feature = new AMap.Polyline({ map: map, path: path, extData: data, zIndex: 2,
//                                 strokeWeight: data.strokeWeight, strokeColor: data.strokeColor, strokeOpacity: data.strokeOpacity });
//                             break;
//                         case "Polygon":
//                             for(j = 0, jl = data.lnglat.length, path = []; j < jl; j++){
//                                 path.push(new AMap.LngLat(data.lnglat[j].lng, data.lnglat[j].lat));
//                             }
//                             feature = new AMap.Polygon({ map: map, path: path, extData: data, zIndex: 1,
//                                 strokeWeight: data.strokeWeight, strokeColor: data.strokeColor, strokeOpacity: data.strokeOpacity,
//                                 fillColor: data.fillColor, fillOpacity: data.fillOpacity });
//                             break;
//                         default: feature = null;
//                     }
//                     if(feature){ AMap.event.addListener(feature, "click", mapFeatureClick); }
//                 }
//             }
//
//             function mapFeatureClick(e){
//                 if(!infoWindow){ infoWindow = new AMap.InfoWindow({autoMove: true}); }
//                 var extData = e.target.getExtData();
//                 infoWindow.setContent("<h5>" + extData.name + "</h5><div>" + extData.desc + "</div>");
//                 infoWindow.open(map, e.lnglat);
//             }
//
//             map = new AMap.Map("mapContainer", {center: new AMap.LngLat(center.lng, center.lat), level: level});
//
//             loadFeatures();
//
//             map.on('complete', function(){
//                 map.plugin(["AMap.ToolBar", "AMap.OverView", "AMap.Scale"], function(){
//                     map.addControl(new AMap.ToolBar);
//                     map.addControl(new AMap.OverView({isOpen: true}));
//                     map.addControl(new AMap.Scale);
//                 });
//             })
//
//         }();
//     }
// })();
